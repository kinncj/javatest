import java.util.*;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class App {

    @RequestMapping(method=RequestMethod.GET, value="/users")
    public Set<User> listUsers() {
        UserRepository repo = UserRepository.getInstance();

        return repo.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/users")
    public Set<User> addUser(@ModelAttribute User user) {
        UserRepository repo = UserRepository.getInstance();

        repo.add(user);

        return repo.findAll();
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);
    }

}
