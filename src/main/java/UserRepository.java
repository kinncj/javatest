import java.util.*;

class UserRepository {
    private static UserRepository instance = null;
    private Set<User> userList = new HashSet<User>();

    public static UserRepository getInstance() {
        if (null == instance) {
            instance = new UserRepository();
        }

        return instance;
    }

    public Set<User> findAll() {
        return userList;
    }

    public void add(User user) {
        userList.add(user);
    }
}
